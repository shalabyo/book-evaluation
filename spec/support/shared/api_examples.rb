shared_context 'API Context' do
  let(:hi) do
    { 'object' => 'page',
      'entry' => [
        { 'id' => '245049719565171',
          'time' => Time.now.to_i,
          'messaging' => [
            { 'sender' => { 'id' => "#{messenger_id}" },
              'recipient' => { 'id' => '245049719565171' },
              'timestamp' => Time.now.to_i,
              'message' => {
                'mid' => 'mid.$cAADe3wtooWVpqwbDXljecF6o-4-E',
                'seq' => 130_005,
                'text' => 'hi'
              } }
          ] }
      ] }
  end

  let(:get_started) do
    { 'object' => 'page',
      'entry' => [
        { 'id' => '245049719565171',
          'time' => Time.now.to_i,
          'messaging' => [
            { 'recipient' => { 'id' => '245049719565171' },
              'timestamp' => Time.now.to_i,
              'sender' => { 'id' => "#{messenger_id}" },
              'postback' => { 'payload' => 'START_USER_SURVEY',
                              'title' => 'Get Started' } }
          ] }
      ] }
  end

  let(:choose_name) do
    { 'object' => 'page',
      'entry' => [
        { 'id' => '245049719565171',
          'time' => Time.now.to_i,
          'messaging' => [
            { 'recipient' => { 'id' => '245049719565171' },
              'timestamp' => Time.now.to_i,
              'sender' => { 'id' => "#{messenger_id}" },
              'postback' => { 'payload' => 'BY_NAME',
                              'title' => 'Name' } }
          ] }
      ] }
  end

  let(:choose_id) do
    { 'object' => 'page',
      'entry' => [
        { 'id' => '245049719565171',
          'time' => Time.now.to_i,
          'messaging' => [
            { 'recipient' => { 'id' => '245049719565171' },
              'timestamp' => Time.now.to_i,
              'sender' => { 'id' => "#{messenger_id}" },
              'postback' => { 'payload' => 'BY_ID',
                              'title' => 'Goodreads ID' } }
          ] }
      ] }
  end

  let(:input_name) do
    { 'object' => 'page',
      'entry' => [
        { 'id' => '245049719565171',
          'time' => Time.now.to_i,
          'messaging' => [
            { 'sender' => { 'id' => "#{messenger_id}" },
              'recipient' => { 'id' => '245049719565171' },
              'timestamp' => Time.now.to_i,
              'message' => {
                'mid' => 'mid.$cAADe3wtooWVpsNa3vVjf5FvKUB-G',
                'seq' => 130_081,
                'text' => 'lord of the rings'
              } }
          ] }
      ] }
  end

  let(:input_id) do
    { 'object' => 'page',
      'entry' => [
        { 'id' => '245049719565171',
          'time' => Time.now.to_i,
          'messaging' => [
            { 'sender' => { 'id' => "#{messenger_id}" },
              'recipient' => { 'id' => '245049719565171' },
              'timestamp' => Time.now.to_i,
              'message' => {
                'mid' => 'mid.$cAADe3wtooWVpsNa3vVjf5FvKUB-G',
                'seq' => 130_081,
                'text' => '33'
              } }
          ] }
      ] }
  end

  let(:select_book) do
    { 'object' => 'page',
      'entry' => [
        { 'id' => '245049719565171',
          'time' => Time.now.to_i,
          'messaging' => [
            { 'recipient' => { 'id' => '245049719565171' },
              'timestamp' => Time.now.to_i,
              'sender' => { 'id' => "#{messenger_id}" },
              'postback' => { 'payload' => '33',
                              'title' => 'The Lord of the Ring...' } }
          ] }
      ] }
  end

  let(:confirm_book) do
    { 'object' => 'page',
      'entry' => [
        { 'id' => '245049719565171',
          'time' => Time.now.to_i,
          'messaging' => [
            { 'recipient' => { 'id' => '245049719565171' },
              'timestamp' => Time.now.to_i,
              'sender' => { 'id' => "#{messenger_id}" },
              'postback' => { 'payload' => '33',
                              'title' => 'Confirm' } }
          ] }
      ] }
  end

  let(:cancel) do
    { 'object' => 'page',
      'entry' => [
        { 'id' => '245049719565171',
          'time' => Time.now.to_i,
          'messaging' => [
            { 'recipient' => { 'id' => '245049719565171' },
              'timestamp' => Time.now.to_i,
              'sender' => { 'id' => "#{messenger_id}" },
              'postback' => { 'payload' => 'ASK_FIRST_QUESTION',
                              'title' => 'Cancel' } }
          ] }
      ] }
  end

  let(:user) { create(:user, :quit) }

  let :first_name do
    user.try(:first_name) || "Name"
  end

  let :messenger_id do
    user.try(:messenger_id) || Faker::Number.number(16)
  end

  let :get_name do
    stub_request(:get, "https://graph.facebook.com/v2.6/#{messenger_id}?access_token=#{ENV['ACCESS_TOKEN']}&fields=first_name")
      .to_return(status: 200, body: JSON.dump('first_name' => "#{first_name}", 'id' => '3'), headers: {})
  end

  let :greet do
    stub_request(:post, "https://graph.facebook.com/v2.6/me/messages?access_token=#{ENV['ACCESS_TOKEN']}")
      .with(
        body: JSON.dump({"recipient":{"id":"#{messenger_id}"},"message":{"text":"Hi #{first_name}"}}),
        headers: {
          'Content-Type' => 'application/json'
        }
      )
      .to_return(status: 200, body: '', headers: {})
  end

  let :first_question do
    stub_request(:post, "https://graph.facebook.com/v2.6/me/messages?access_token=#{ENV['ACCESS_TOKEN']}")
      .with(
        body: JSON.dump({"recipient":{"id":"#{messenger_id}"},"message":{"attachment":{"type":"template","payload":{"template_type":"button","text":"Are you searching for a book by...?","buttons":[{"type":"postback","title":"Name","payload":"BY_NAME"},{"type":"postback","title":"Goodreads ID","payload":"BY_ID"}]}}}}),
        headers: {
          'Content-Type' => 'application/json'
        }
      )
      .to_return(status: 200, body: '', headers: {})
  end

  let :by_name do
    stub_request(:post, "https://graph.facebook.com/v2.6/me/messages?access_token=#{ENV['ACCESS_TOKEN']}")
      .with(
        body: JSON.dump({ "recipient":{ "id": "#{messenger_id}" }, "message": { "text": "Enter book name" } }),
        headers: {
          'Content-Type' => 'application/json'
        }
      )
      .to_return(status: 200, body: '', headers: {})
  end

  let :by_id do
    stub_request(:post, "https://graph.facebook.com/v2.6/me/messages?access_token=#{ENV['ACCESS_TOKEN']}")
      .with(
        body: JSON.dump({ "recipient": { "id": "#{messenger_id}" }, "message": { "text": "Enter book id" } }),
        headers: {
          'Content-Type' => 'application/json'
        }
      )
      .to_return(status: 200, body: '', headers: {})
  end

  let :search_body do
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<GoodreadsResponse>\n  <Request>\n    <authentication>true</authentication>\n      <key><![CDATA[ZU7ZeXjsNSwNoq0pB6EZA]]></key>\n    <method><![CDATA[search_index]]></method>\n  </Request>\n  <search>\n  <query><![CDATA[lord of the rings]]></query>\n    <results-start>1</results-start>\n    <results-end>20</results-end>\n    <total-results>644</total-results>\n    <source>Goodreads</source>\n    <query-time-seconds>0.16</query-time-seconds>\n    <results>\n        <work>\n  <id type=\"integer\">3462456</id>\n  <books_count type=\"integer\">173</books_count>\n  <ratings_count type=\"integer\">464311</ratings_count>\n  <text_reviews_count type=\"integer\">10000</text_reviews_count>\n  <original_publication_year type=\"integer\">1955</original_publication_year>\n  <original_publication_month type=\"integer\">10</original_publication_month>\n  <original_publication_day type=\"integer\">20</original_publication_day>\n  <average_rating>4.48</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">33</id>\n    <title>The Lord of the Rings (The Lord of the Rings, #1-3)</title>\n    <author>\n      <id type=\"integer\">656983</id>\n      <name>J.R.R. Tolkien</name>\n    </author>\n    <image_url>https://images.gr-assets.com/books/1521850178m/33.jpg</image_url>\n    <small_image_url>https://images.gr-assets.com/books/1521850178s/33.jpg</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">3204327</id>\n  <books_count type=\"integer\">500</books_count>\n  <ratings_count type=\"integer\">1953228</ratings_count>\n  <text_reviews_count type=\"integer\">16235</text_reviews_count>\n  <original_publication_year type=\"integer\">1954</original_publication_year>\n  <original_publication_month type=\"integer\">7</original_publication_month>\n  <original_publication_day type=\"integer\">29</original_publication_day>\n  <average_rating>4.34</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">34</id>\n    <title>The Fellowship of the Ring (The Lord of the Rings, #1)</title>\n    <author>\n      <id type=\"integer\">656983</id>\n      <name>J.R.R. Tolkien</name>\n    </author>\n    <image_url>https://images.gr-assets.com/books/1298411339m/34.jpg</image_url>\n    <small_image_url>https://images.gr-assets.com/books/1298411339s/34.jpg</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">2963845</id>\n  <books_count type=\"integer\">440</books_count>\n  <ratings_count type=\"integer\">564593</ratings_count>\n  <text_reviews_count type=\"integer\">7580</text_reviews_count>\n  <original_publication_year type=\"integer\">1954</original_publication_year>\n  <original_publication_month type=\"integer\">11</original_publication_month>\n  <original_publication_day type=\"integer\">11</original_publication_day>\n  <average_rating>4.42</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">15241</id>\n    <title>The Two Towers (The Lord of the Rings, #2)</title>\n    <author>\n      <id type=\"integer\">656983</id>\n      <name>J.R.R. Tolkien</name>\n    </author>\n    <image_url>https://images.gr-assets.com/books/1298415523m/15241.jpg</image_url>\n    <small_image_url>https://images.gr-assets.com/books/1298415523s/15241.jpg</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">2964424</id>\n  <books_count type=\"integer\">436</books_count>\n  <ratings_count type=\"integer\">541609</ratings_count>\n  <text_reviews_count type=\"integer\">7228</text_reviews_count>\n  <original_publication_year type=\"integer\">1955</original_publication_year>\n  <original_publication_month type=\"integer\">10</original_publication_month>\n  <original_publication_day type=\"integer\">20</original_publication_day>\n  <average_rating>4.51</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">18512</id>\n    <title>The Return of the King (The Lord of the Rings, #3)</title>\n    <author>\n      <id type=\"integer\">656983</id>\n      <name>J.R.R. Tolkien</name>\n    </author>\n    <image_url>https://images.gr-assets.com/books/1520258755m/18512.jpg</image_url>\n    <small_image_url>https://images.gr-assets.com/books/1520258755s/18512.jpg</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">4414</id>\n  <books_count type=\"integer\">11</books_count>\n  <ratings_count type=\"integer\">19069</ratings_count>\n  <text_reviews_count type=\"integer\">50</text_reviews_count>\n  <original_publication_year type=\"integer\" nil=\"true\"/>\n  <original_publication_month type=\"integer\" nil=\"true\"/>\n  <original_publication_day type=\"integer\" nil=\"true\"/>\n  <average_rating>4.53</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">36</id>\n    <title>The Lord of the Rings: Weapons and Warfare</title>\n    <author>\n      <id type=\"integer\">5448409</id>\n      <name>Chris   Smith</name>\n    </author>\n    <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>\n    <small_image_url>https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">4479</id>\n  <books_count type=\"integer\">7</books_count>\n  <ratings_count type=\"integer\">24622</ratings_count>\n  <text_reviews_count type=\"integer\">88</text_reviews_count>\n  <original_publication_year type=\"integer\">2002</original_publication_year>\n  <original_publication_month type=\"integer\">6</original_publication_month>\n  <original_publication_day type=\"integer\">12</original_publication_day>\n  <average_rating>4.59</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">119</id>\n    <title>The Lord of the Rings: The Art of The Fellowship of the Ring</title>\n    <author>\n      <id type=\"integer\">60</id>\n      <name>Gary Russell</name>\n    </author>\n    <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>\n    <small_image_url>https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">26510</id>\n  <books_count type=\"integer\">6</books_count>\n  <ratings_count type=\"integer\">9447</ratings_count>\n  <text_reviews_count type=\"integer\">74</text_reviews_count>\n  <original_publication_year type=\"integer\">2005</original_publication_year>\n  <original_publication_month type=\"integer\">1</original_publication_month>\n  <original_publication_day type=\"integer\">1</original_publication_day>\n  <average_rating>4.26</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">25790</id>\n    <title>The Lord of the Rings Sketchbook</title>\n    <author>\n      <id type=\"integer\">9545</id>\n      <name>Alan Lee</name>\n    </author>\n    <image_url>https://images.gr-assets.com/books/1416453209m/25790.jpg</image_url>\n    <small_image_url>https://images.gr-assets.com/books/1416453209s/25790.jpg</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">89369</id>\n  <books_count type=\"integer\">9</books_count>\n  <ratings_count type=\"integer\">98241</ratings_count>\n  <text_reviews_count type=\"integer\">1628</text_reviews_count>\n  <original_publication_year type=\"integer\">1955</original_publication_year>\n  <original_publication_month type=\"integer\">10</original_publication_month>\n  <original_publication_day type=\"integer\">20</original_publication_day>\n  <average_rating>4.59</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">30</id>\n    <title>J.R.R. Tolkien 4-Book Boxed Set: The Hobbit and The Lord of the Rings</title>\n    <author>\n      <id type=\"integer\">656983</id>\n      <name>J.R.R. Tolkien</name>\n    </author>\n    <image_url>https://images.gr-assets.com/books/1346072396m/30.jpg</image_url>\n    <small_image_url>https://images.gr-assets.com/books/1346072396s/30.jpg</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">17136</id>\n  <books_count type=\"integer\">5</books_count>\n  <ratings_count type=\"integer\">4447</ratings_count>\n  <text_reviews_count type=\"integer\">23</text_reviews_count>\n  <original_publication_year type=\"integer\">2005</original_publication_year>\n  <original_publication_month type=\"integer\">10</original_publication_month>\n  <original_publication_day type=\"integer\">17</original_publication_day>\n  <average_rating>4.35</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">15232</id>\n    <title>The Lord of the Rings: A Reader's Companion</title>\n    <author>\n      <id type=\"integer\">9498</id>\n      <name>Wayne G. Hammond</name>\n    </author>\n    <image_url>https://images.gr-assets.com/books/1396781282m/15232.jpg</image_url>\n    <small_image_url>https://images.gr-assets.com/books/1396781282s/15232.jpg</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">521513</id>\n  <books_count type=\"integer\">14</books_count>\n  <ratings_count type=\"integer\">7637</ratings_count>\n  <text_reviews_count type=\"integer\">40</text_reviews_count>\n  <original_publication_year type=\"integer\">2002</original_publication_year>\n  <original_publication_month type=\"integer\" nil=\"true\"/>\n  <original_publication_day type=\"integer\" nil=\"true\"/>\n  <average_rating>4.46</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">7351</id>\n    <title>The Lord of the Rings: The Making of the Movie Trilogy</title>\n    <author>\n      <id type=\"integer\">4941</id>\n      <name>Brian Sibley</name>\n    </author>\n    <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>\n    <small_image_url>https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">65478</id>\n  <books_count type=\"integer\">6</books_count>\n  <ratings_count type=\"integer\">7613</ratings_count>\n  <text_reviews_count type=\"integer\">31</text_reviews_count>\n  <original_publication_year type=\"integer\">2004</original_publication_year>\n  <original_publication_month type=\"integer\">1</original_publication_month>\n  <original_publication_day type=\"integer\">1</original_publication_day>\n  <average_rating>4.53</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">67514</id>\n    <title>The Lord of the Rings: The Art of The Return of the King</title>\n    <author>\n      <id type=\"integer\">60</id>\n      <name>Gary Russell</name>\n    </author>\n    <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>\n    <small_image_url>https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">17145</id>\n  <books_count type=\"integer\">8</books_count>\n  <ratings_count type=\"integer\">7527</ratings_count>\n  <text_reviews_count type=\"integer\">27</text_reviews_count>\n  <original_publication_year type=\"integer\">2003</original_publication_year>\n  <original_publication_month type=\"integer\" nil=\"true\"/>\n  <original_publication_day type=\"integer\" nil=\"true\"/>\n  <average_rating>4.57</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">15242</id>\n    <title>The Lord of the Rings: The Art of The Two Towers</title>\n    <author>\n      <id type=\"integer\">60</id>\n      <name>Gary Russell</name>\n    </author>\n    <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>\n    <small_image_url>https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">17150</id>\n  <books_count type=\"integer\">10</books_count>\n  <ratings_count type=\"integer\">17618</ratings_count>\n  <text_reviews_count type=\"integer\">29</text_reviews_count>\n  <original_publication_year type=\"integer\" nil=\"true\"/>\n  <original_publication_month type=\"integer\" nil=\"true\"/>\n  <original_publication_day type=\"integer\" nil=\"true\"/>\n  <average_rating>4.59</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">349254</id>\n    <title>The Lord of the Rings: The Return of the King: Visual Companion</title>\n    <author>\n      <id type=\"integer\">10</id>\n      <name>Jude Fisher</name>\n    </author>\n    <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>\n    <small_image_url>https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">1637201</id>\n  <books_count type=\"integer\">10</books_count>\n  <ratings_count type=\"integer\">6161</ratings_count>\n  <text_reviews_count type=\"integer\">27</text_reviews_count>\n  <original_publication_year type=\"integer\">2001</original_publication_year>\n  <original_publication_month type=\"integer\" nil=\"true\"/>\n  <original_publication_day type=\"integer\" nil=\"true\"/>\n  <average_rating>4.38</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">15239</id>\n    <title>The Lord of the Rings: Official Movie Guide</title>\n    <author>\n      <id type=\"integer\">4941</id>\n      <name>Brian Sibley</name>\n    </author>\n    <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>\n    <small_image_url>https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">306544</id>\n  <books_count type=\"integer\">48</books_count>\n  <ratings_count type=\"integer\">5301</ratings_count>\n  <text_reviews_count type=\"integer\">396</text_reviews_count>\n  <original_publication_year type=\"integer\">1969</original_publication_year>\n  <original_publication_month type=\"integer\" nil=\"true\"/>\n  <original_publication_day type=\"integer\" nil=\"true\"/>\n  <average_rating>3.12</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">15348</id>\n    <title>Bored of the Rings: A Parody of J.R.R. Tolkien's Lord of the Rings</title>\n    <author>\n      <id type=\"integer\">4616903</id>\n      <name>The Harvard Lampoon</name>\n    </author>\n    <image_url>https://images.gr-assets.com/books/1309287397m/15348.jpg</image_url>\n    <small_image_url>https://images.gr-assets.com/books/1309287397s/15348.jpg</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">521514</id>\n  <books_count type=\"integer\">12</books_count>\n  <ratings_count type=\"integer\">789</ratings_count>\n  <text_reviews_count type=\"integer\">58</text_reviews_count>\n  <original_publication_year type=\"integer\">2001</original_publication_year>\n  <original_publication_month type=\"integer\" nil=\"true\"/>\n  <original_publication_day type=\"integer\" nil=\"true\"/>\n  <average_rating>3.78</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">534003</id>\n    <title>Finding God in the Lord of the Rings</title>\n    <author>\n      <id type=\"integer\">59187</id>\n      <name>Kurt Bruner</name>\n    </author>\n    <image_url>https://images.gr-assets.com/books/1406506192m/534003.jpg</image_url>\n    <small_image_url>https://images.gr-assets.com/books/1406506192s/534003.jpg</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">808427</id>\n  <books_count type=\"integer\">12</books_count>\n  <ratings_count type=\"integer\">4973</ratings_count>\n  <text_reviews_count type=\"integer\">35</text_reviews_count>\n  <original_publication_year type=\"integer\">2001</original_publication_year>\n  <original_publication_month type=\"integer\">1</original_publication_month>\n  <original_publication_day type=\"integer\">1</original_publication_day>\n  <average_rating>4.47</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">15222</id>\n    <title>The Lord of the Rings: The Fellowship of the Ring: Visual Companion</title>\n    <author>\n      <id type=\"integer\">10</id>\n      <name>Jude Fisher</name>\n    </author>\n    <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>\n    <small_image_url>https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">90550</id>\n  <books_count type=\"integer\">11</books_count>\n  <ratings_count type=\"integer\">4531</ratings_count>\n  <text_reviews_count type=\"integer\">28</text_reviews_count>\n  <original_publication_year type=\"integer\" nil=\"true\"/>\n  <original_publication_month type=\"integer\" nil=\"true\"/>\n  <original_publication_day type=\"integer\" nil=\"true\"/>\n  <average_rating>4.51</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">15221</id>\n    <title>The Lord of the Rings: The Two Towers: Visual Companion</title>\n    <author>\n      <id type=\"integer\">10</id>\n      <name>Jude Fisher</name>\n    </author>\n    <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>\n    <small_image_url>https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">68766</id>\n  <books_count type=\"integer\">8</books_count>\n  <ratings_count type=\"integer\">643</ratings_count>\n  <text_reviews_count type=\"integer\">27</text_reviews_count>\n  <original_publication_year type=\"integer\">2002</original_publication_year>\n  <original_publication_month type=\"integer\">1</original_publication_month>\n  <original_publication_day type=\"integer\">1</original_publication_day>\n  <average_rating>4.26</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">70990</id>\n    <title>The Lord of the Rings Location Guidebook</title>\n    <author>\n      <id type=\"integer\">16004017</id>\n      <name>Ian  Brodie</name>\n    </author>\n    <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>\n    <small_image_url>https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png</small_image_url>\n  </best_book>\n</work>\n\n        <work>\n  <id type=\"integer\">17141</id>\n  <books_count type=\"integer\">4</books_count>\n  <ratings_count type=\"integer\">1060</ratings_count>\n  <text_reviews_count type=\"integer\">11</text_reviews_count>\n  <original_publication_year type=\"integer\">2004</original_publication_year>\n  <original_publication_month type=\"integer\">1</original_publication_month>\n  <original_publication_day type=\"integer\">1</original_publication_day>\n  <average_rating>4.52</average_rating>\n  <best_book type=\"Book\">\n    <id type=\"integer\">15237</id>\n    <title>The Art of The Lord of the Rings</title>\n    <author>\n      <id type=\"integer\">60</id>\n      <name>Gary Russell</name>\n    </author>\n    <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>\n    <small_image_url>https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png</small_image_url>\n  </best_book>\n</work>\n\n    </results>\n</search>\n\n</GoodreadsResponse>"
  end

  let :find_body do
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<GoodreadsResponse>\n  <Request>\n    <authentication>true</authentication>\n      <key><![CDATA[ZU7ZeXjsNSwNoq0pB6EZA]]></key>\n    <method><![CDATA[book_show]]></method>\n  </Request>\n  <book>\n  <id>33</id>\n  <title><![CDATA[The Lord of the Rings (The Lord of the Rings, #1-3)]]></title>\n  <isbn><![CDATA[0618640150]]></isbn>\n  <isbn13><![CDATA[9780618640157]]></isbn13>\n  <asin><![CDATA[]]></asin>\n  <kindle_asin><![CDATA[]]></kindle_asin>\n  <marketplace_id><![CDATA[]]></marketplace_id>\n  <country_code><![CDATA[EG]]></country_code>\n  <image_url>https://images.gr-assets.com/books/1521850178m/33.jpg</image_url>\n  <small_image_url>https://images.gr-assets.com/books/1521850178s/33.jpg</small_image_url>\n  <publication_year>2005</publication_year>\n  <publication_month>10</publication_month>\n  <publication_day>12</publication_day>\n  <publisher>Houghton Mifflin Harcourt</publisher>\n  <language_code>eng</language_code>\n  <is_ebook>false</is_ebook>\n  <description><![CDATA[<b>One Ring to rule them all, One Ring to find them, One Ring to bring them all and in the darkness bind them</b><br /><br />In ancient times the Rings of Power were crafted by the Elven-smiths, and Sauron, the Dark Lord, forged the One Ring, filling it with his own power so that he could rule all others. But the One Ring was taken from him, and though he sought it throughout Middle-earth, it remained lost to him. After many ages it fell by chance into the hands of the hobbit Bilbo Baggins.<br /><br />From Sauron's fastness in the Dark Tower of Mordor, his power spread far and wide. Sauron gathered all the Great Rings to him, but always he searched for the One Ring that would complete his dominion.<br /><br />When Bilbo reached his eleventy-first birthday he disappeared, bequeathing to his young cousin Frodo the Ruling Ring and a perilous quest: to journey across Middle-earth, deep into the shadow of the Dark Lord, and destroy the Ring by casting it into the Cracks of Doom.<br /><br /><i>The Lord of the Rings</i> tells of the great quest undertaken by Frodo and the Fellowship of the Ring: Gandalf the Wizard; the hobbits Merry, Pippin, and Sam; Gimli the Dwarf; Legolas the Elf; Boromir of Gondor; and a tall, mysterious stranger called Strider.]]></description>\n  <work>\n  <id type=\"integer\">3462456</id>\n  <books_count type=\"integer\">173</books_count>\n  <best_book_id type=\"integer\">33</best_book_id>\n  <reviews_count type=\"integer\">677302</reviews_count>\n  <ratings_sum type=\"integer\">2080706</ratings_sum>\n  <ratings_count type=\"integer\">464343</ratings_count>\n  <text_reviews_count type=\"integer\">10000</text_reviews_count>\n  <original_publication_year type=\"integer\">1955</original_publication_year>\n  <original_publication_month type=\"integer\">10</original_publication_month>\n  <original_publication_day type=\"integer\">20</original_publication_day>\n  <original_title>The Lord of the Rings</original_title>\n  <original_language_id type=\"integer\" nil=\"true\"/>\n  <media_type>book</media_type>\n  <rating_dist>5:297527|4:112565|3:39277|2:10006|1:4968|total:464343</rating_dist>\n  <desc_user_id type=\"integer\">66648866</desc_user_id>\n  <default_chaptering_book_id type=\"integer\" nil=\"true\"/>\n  <default_description_language_code nil=\"true\"/>\n</work>\n  <average_rating>4.48</average_rating>\n  <num_pages><![CDATA[1216]]></num_pages>\n  <format><![CDATA[Paperback]]></format>\n  <edition_information><![CDATA[50th Anniversary One-volume Edition]]></edition_information>\n  <ratings_count><![CDATA[410971]]></ratings_count>\n  <text_reviews_count><![CDATA[7558]]></text_reviews_count>\n  <url><![CDATA[https://www.goodreads.com/book/show/33.The_Lord_of_the_Rings]]></url>\n  <link><![CDATA[https://www.goodreads.com/book/show/33.The_Lord_of_the_Rings]]></link>\n  <authors>\n<author>\n<id>656983</id>\n<name>J.R.R. Tolkien</name>\n<role></role>\n<image_url nophoto='false'>\n<![CDATA[https://images.gr-assets.com/authors/1434625177p5/656983.jpg]]>\n</image_url>\n<small_image_url nophoto='false'>\n<![CDATA[https://images.gr-assets.com/authors/1434625177p2/656983.jpg]]>\n</small_image_url>\n<link><![CDATA[https://www.goodreads.com/author/show/656983.J_R_R_Tolkien]]></link>\n<average_rating>4.31</average_rating>\n<ratings_count>6756292</ratings_count>\n<text_reviews_count>102275</text_reviews_count>\n</author>\n</authors>\n\n    <reviews_widget>\n      <![CDATA[\n        <style>\n  #goodreads-widget {\n    font-family: georgia, serif;\n    padding: 18px 0;\n    width:565px;\n  }\n  #goodreads-widget h1 {\n    font-weight:normal;\n    font-size: 16px;\n    border-bottom: 1px solid #BBB596;\n    margin-bottom: 0;\n  }\n  #goodreads-widget a {\n    text-decoration: none;\n    color:#660;\n  }\n  iframe{\n    background-color: #fff;\n  }\n  #goodreads-widget a:hover { text-decoration: underline; }\n  #goodreads-widget a:active {\n    color:#660;\n  }\n  #gr_footer {\n    width: 100%;\n    border-top: 1px solid #BBB596;\n    text-align: right;\n  }\n  #goodreads-widget .gr_branding{\n    color: #382110;\n    font-size: 11px;\n    text-decoration: none;\n    font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n  }\n</style>\n<div id=\"goodreads-widget\">\n  <div id=\"gr_header\"><h1><a rel=\"nofollow\" href=\"https://www.goodreads.com/book/show/33.The_Lord_of_the_Rings\">The Lord of the Rings Reviews</a></h1></div>\n  <iframe id=\"the_iframe\" src=\"https://www.goodreads.com/api/reviews_widget_iframe?did=DEVELOPER_ID&amp;format=html&amp;isbn=0618640150&amp;links=660&amp;min_rating=&amp;review_back=fff&amp;stars=000&amp;text=000\" width=\"565\" height=\"400\" frameborder=\"0\"></iframe>\n  <div id=\"gr_footer\">\n    <a class=\"gr_branding\" target=\"_blank\" rel=\"nofollow\" href=\"https://www.goodreads.com/book/show/33.The_Lord_of_the_Rings?utm_medium=api&amp;utm_source=reviews_widget\">Reviews from Goodreads.com</a>\n  </div>\n</div>\n\n      ]]>\n    </reviews_widget>\n  <popular_shelves>\n      <shelf name=\"to-read\" count=\"131842\"/>\n      <shelf name=\"currently-reading\" count=\"16923\"/>\n      <shelf name=\"fantasy\" count=\"14369\"/>\n      <shelf name=\"favorites\" count=\"8560\"/>\n      <shelf name=\"classics\" count=\"4137\"/>\n      <shelf name=\"fiction\" count=\"4061\"/>\n      <shelf name=\"owned\" count=\"1476\"/>\n      <shelf name=\"books-i-own\" count=\"1090\"/>\n      <shelf name=\"favourites\" count=\"870\"/>\n      <shelf name=\"classic\" count=\"813\"/>\n      <shelf name=\"adventure\" count=\"622\"/>\n      <shelf name=\"tolkien\" count=\"621\"/>\n      <shelf name=\"sci-fi-fantasy\" count=\"584\"/>\n      <shelf name=\"default\" count=\"531\"/>\n      <shelf name=\"series\" count=\"477\"/>\n      <shelf name=\"owned-books\" count=\"410\"/>\n      <shelf name=\"epic-fantasy\" count=\"396\"/>\n      <shelf name=\"literature\" count=\"369\"/>\n      <shelf name=\"high-fantasy\" count=\"350\"/>\n      <shelf name=\"novels\" count=\"321\"/>\n      <shelf name=\"fantasy-sci-fi\" count=\"301\"/>\n      <shelf name=\"young-adult\" count=\"288\"/>\n      <shelf name=\"all-time-favorites\" count=\"272\"/>\n      <shelf name=\"my-books\" count=\"250\"/>\n      <shelf name=\"1001-books\" count=\"243\"/>\n      <shelf name=\"epic\" count=\"234\"/>\n      <shelf name=\"re-read\" count=\"220\"/>\n      <shelf name=\"my-library\" count=\"214\"/>\n      <shelf name=\"favorite-books\" count=\"212\"/>\n      <shelf name=\"scifi-fantasy\" count=\"212\"/>\n      <shelf name=\"magic\" count=\"201\"/>\n      <shelf name=\"library\" count=\"190\"/>\n      <shelf name=\"british\" count=\"184\"/>\n      <shelf name=\"favorite\" count=\"176\"/>\n      <shelf name=\"sci-fi\" count=\"175\"/>\n      <shelf name=\"kindle\" count=\"171\"/>\n      <shelf name=\"j-r-r-tolkien\" count=\"170\"/>\n      <shelf name=\"science-fiction-fantasy\" count=\"164\"/>\n      <shelf name=\"science-fiction\" count=\"156\"/>\n      <shelf name=\"fantasy-scifi\" count=\"154\"/>\n      <shelf name=\"english\" count=\"151\"/>\n      <shelf name=\"novel\" count=\"150\"/>\n      <shelf name=\"to-buy\" count=\"141\"/>\n      <shelf name=\"1001\" count=\"138\"/>\n      <shelf name=\"middle-earth\" count=\"132\"/>\n      <shelf name=\"i-own\" count=\"132\"/>\n      <shelf name=\"adult\" count=\"131\"/>\n      <shelf name=\"shelfari-favorites\" count=\"130\"/>\n      <shelf name=\"books\" count=\"129\"/>\n      <shelf name=\"childhood\" count=\"115\"/>\n      <shelf name=\"lord-of-the-rings\" count=\"107\"/>\n      <shelf name=\"fantasy-fiction\" count=\"106\"/>\n      <shelf name=\"to-re-read\" count=\"106\"/>\n      <shelf name=\"20th-century\" count=\"105\"/>\n      <shelf name=\"sci-fi-and-fantasy\" count=\"105\"/>\n      <shelf name=\"classic-literature\" count=\"102\"/>\n      <shelf name=\"own-it\" count=\"101\"/>\n      <shelf name=\"fantasia\" count=\"97\"/>\n      <shelf name=\"5-stars\" count=\"95\"/>\n      <shelf name=\"ebook\" count=\"91\"/>\n      <shelf name=\"adult-fiction\" count=\"91\"/>\n      <shelf name=\"my-favorites\" count=\"88\"/>\n      <shelf name=\"1001-books-to-read-before-you-die\" count=\"87\"/>\n      <shelf name=\"abandoned\" count=\"87\"/>\n      <shelf name=\"favourite\" count=\"85\"/>\n      <shelf name=\"all-time-favourites\" count=\"85\"/>\n      <shelf name=\"on-hold\" count=\"84\"/>\n      <shelf name=\"read-more-than-once\" count=\"83\"/>\n      <shelf name=\"finished\" count=\"80\"/>\n      <shelf name=\"faves\" count=\"80\"/>\n      <shelf name=\"unfinished\" count=\"79\"/>\n      <shelf name=\"british-literature\" count=\"79\"/>\n      <shelf name=\"fiction-fantasy\" count=\"79\"/>\n      <shelf name=\"favourite-books\" count=\"78\"/>\n      <shelf name=\"reread\" count=\"78\"/>\n      <shelf name=\"home-library\" count=\"77\"/>\n      <shelf name=\"ebooks\" count=\"75\"/>\n      <shelf name=\"speculative-fiction\" count=\"73\"/>\n      <shelf name=\"ya\" count=\"73\"/>\n      <shelf name=\"must-read\" count=\"73\"/>\n      <shelf name=\"audiobooks\" count=\"72\"/>\n      <shelf name=\"english-literature\" count=\"70\"/>\n      <shelf name=\"fantasy-science-fiction\" count=\"70\"/>\n      <shelf name=\"to-reread\" count=\"69\"/>\n      <shelf name=\"sf-fantasy\" count=\"67\"/>\n      <shelf name=\"bookshelf\" count=\"64\"/>\n      <shelf name=\"modern-classics\" count=\"62\"/>\n      <shelf name=\"re-reading\" count=\"62\"/>\n      <shelf name=\"fantasy-and-sci-fi\" count=\"61\"/>\n      <shelf name=\"jrr-tolkien\" count=\"61\"/>\n      <shelf name=\"my-bookshelf\" count=\"59\"/>\n      <shelf name=\"have\" count=\"59\"/>\n      <shelf name=\"audiobook\" count=\"59\"/>\n      <shelf name=\"movies\" count=\"58\"/>\n      <shelf name=\"mythology\" count=\"57\"/>\n      <shelf name=\"re-reads\" count=\"56\"/>\n      <shelf name=\"war\" count=\"56\"/>\n      <shelf name=\"1001-books-you-must-read-before-you\" count=\"56\"/>\n      <shelf name=\"wish-list\" count=\"55\"/>\n      <shelf name=\"classics-to-read\" count=\"54\"/>\n  </popular_shelves>\n  <book_links>\n    <book_link>\n  <id>8</id>\n  <name>Libraries</name>\n  <link>https://www.goodreads.com/book_link/follow/8</link>\n</book_link>\n\n  </book_links>\n  <buy_links>\n    <buy_link>\n<id>1</id>\n<name>Amazon</name>\n<link>https://www.goodreads.com/book_link/follow/1</link>\n</buy_link>\n<buy_link>\n<id>10</id>\n<name>Audible</name>\n<link>https://www.goodreads.com/book_link/follow/10</link>\n</buy_link>\n<buy_link>\n<id>3</id>\n<name>Barnes &amp; Noble</name>\n<link>https://www.goodreads.com/book_link/follow/3</link>\n</buy_link>\n<buy_link>\n<id>1027</id>\n<name>Kobo</name>\n<link>https://www.goodreads.com/book_link/follow/1027</link>\n</buy_link>\n<buy_link>\n<id>2102</id>\n<name>Apple iBooks</name>\n<link>https://www.goodreads.com/book_link/follow/2102</link>\n</buy_link>\n<buy_link>\n<id>8036</id>\n<name>Google Play</name>\n<link>https://www.goodreads.com/book_link/follow/8036</link>\n</buy_link>\n<buy_link>\n<id>4</id>\n<name>Abebooks</name>\n<link>https://www.goodreads.com/book_link/follow/4</link>\n</buy_link>\n<buy_link>\n<id>882</id>\n<name>Book Depository</name>\n<link>https://www.goodreads.com/book_link/follow/882</link>\n</buy_link>\n<buy_link>\n<id>9</id>\n<name>Indigo</name>\n<link>https://www.goodreads.com/book_link/follow/9</link>\n</buy_link>\n<buy_link>\n<id>5</id>\n<name>Alibris</name>\n<link>https://www.goodreads.com/book_link/follow/5</link>\n</buy_link>\n<buy_link>\n<id>107</id>\n<name>Better World Books</name>\n<link>https://www.goodreads.com/book_link/follow/107</link>\n</buy_link>\n<buy_link>\n<id>7</id>\n<name>IndieBound</name>\n<link>https://www.goodreads.com/book_link/follow/7</link>\n</buy_link>\n\n  </buy_links>\n  <series_works>\n    <series_work>\n<id>306416</id>\n<user_position>1-3</user_position>\n<series>\n<id>66175</id>\n<title>\n<![CDATA[\n    The Lord of the Rings\n]]>\n</title>\n<description>\n<![CDATA[\n    See also:\r\n    * <a href=\"http://www.goodreads.com/series/58079-unfinished-tales\">Unfinished Tales</a>\r\n    * <a href=\"http://www.goodreads.com/series/45924-the-history-of-middle-earth\">The History of Middle-Earth</a>\r\n    \r\n    The Lord of the Rings is an epic high fantasy trilogy written by English philologist and University of Oxford professor J.R.R. Tolkien. The story began as a sequel to Tolkien's earlier, less complex children's fantasy novel [book:The Hobbit|5907] (1937), but eventually developed into a much larger work which formed the basis for the extended <a href=\"http://www.goodreads.com/series/58083-middle-earth-universe\">Middle-Earth Universe</a>. It was written in stages between 1937 and 1949, much of it during World War II. It is the third best-selling novel ever written, with over 150 million copies sold.\r\n    \r\n    Also known as:\r\n    * El Señor de los Anillos\r\n    * Der Herr der Ringe\r\n    * O Senhor dos Anéis\r\n    * Le Seigneur des Anneaux\r\n    * A Gyűrűk Ura\r\n    * Gredzenu pavēlnieks\r\n    * Härskarringen\r\n    * In de Ban van de Ring\r\n    * Gospodar prstenova\r\n    * Pán Prsteňov\r\n    * Pán prstenů\r\n    * Ringenes Herre \r\n    * Il Signore degli Anelli\r\n    * Sõrmuste Isand\r\n    * Stăpânul inelelor\r\n    * Taru sormusten herrasta\r\n    * Władca Pierścieni\r\n    * Yüzüklerin Efendisi\r\n    * Властелинът на пръстените\r\n    * Ο άρχοντας των δαχτυλιδιών\r\n    * ბეჭდების მბრძანებელი\r\n    * 魔戒\r\n    * 指輪物語\r\n    * Gospodar prstanov [Slovenian]\r\n    \r\n    Also published in <a href=\"http://www.goodreads.com/series/58085-the-lord-of-the-rings-seven-book-editions\">seven volumes (six books plus the appendices)</a>.\n]]>\n</description>\n<note>\n<![CDATA[\n    The list of translations is useful for series searches (see http://www.goodreads.com/series for the link)\n]]>\n</note>\n<series_works_count>7</series_works_count>\n<primary_work_count>3</primary_work_count>\n<numbered>true</numbered>\n</series>\n\n</series_work>\n<series_work>\n<id>396612</id>\n<user_position></user_position>\n<series>\n<id>58083</id>\n<title>\n<![CDATA[\n    Middle-Earth Universe\n]]>\n</title>\n<description>\n<![CDATA[\n    Includes:\r\n    * <a href=\"http://www.goodreads.com/series/66175-the-lord-of-the-rings\">The Lord of the Rings</a>\r\n    * <a href=\"http://www.goodreads.com/series/58079-unfinished-tales\">Unfinished Tales</a>\r\n    * <a href=\"http://www.goodreads.com/series/45924-the-history-of-middle-earth\">The History of Middle-Earth</a>\n]]>\n</description>\n<note>\n<![CDATA[\n    Listed in publication order. This is not a series, it is a setting.\n]]>\n</note>\n<series_works_count>45</series_works_count>\n<primary_work_count>25</primary_work_count>\n<numbered>false</numbered>\n</series>\n\n</series_work>\n\n  </series_works>\n  <similar_books>\n          <book>\n<id>66677</id>\n<title><![CDATA[Legacy of the Drow (Legacy of the Drow, #1-4; Legend of Drizzt, #7-10)]]></title>\n<title_without_series>Legacy of the Drow</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/66677.Legacy_of_the_Drow]]></link>\n<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>\n<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>\n<num_pages>1077</num_pages>\n<work>\n<id>271735</id>\n</work>\n<isbn>0786929081</isbn>\n<isbn13>9780786929085</isbn13>\n<average_rating>4.38</average_rating>\n<ratings_count>5861</ratings_count>\n<publication_year>2003</publication_year>\n<publication_month>1</publication_month>\n<publication_day>1</publication_day>\n<authors>\n<author>\n<id>1023510</id>\n<name>R.A. Salvatore</name>\n<link><![CDATA[https://www.goodreads.com/author/show/1023510.R_A_Salvatore]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>7345</id>\n<title>A Tolkien Bestiary</title>\n<title_without_series>A Tolkien Bestiary</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/7345.A_Tolkien_Bestiary]]></link>\n<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>\n<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>\n<num_pages></num_pages>\n<work>\n<id>10473</id>\n</work>\n<isbn>0517120771</isbn>\n<isbn13>9780517120774</isbn13>\n<average_rating>3.99</average_rating>\n<ratings_count>6033</ratings_count>\n<publication_year></publication_year>\n<publication_month></publication_month>\n<publication_day></publication_day>\n<authors>\n<author>\n<id>4945</id>\n<name>David Day</name>\n<link><![CDATA[https://www.goodreads.com/author/show/4945.David_Day]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>68380</id>\n<title><![CDATA[Dragonlance Chronicles (Dragonlance #1-3)]]></title>\n<title_without_series>Dragonlance Chronicles</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/68380.Dragonlance_Chronicles]]></link>\n<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>\n<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>\n<num_pages>1030</num_pages>\n<work>\n<id>849</id>\n</work>\n<isbn>0880386525</isbn>\n<isbn13>9780880386524</isbn13>\n<average_rating>4.21</average_rating>\n<ratings_count>17265</ratings_count>\n<publication_year>1988</publication_year>\n<publication_month>3</publication_month>\n<publication_day>1</publication_day>\n<authors>\n<author>\n<id>869</id>\n<name>Margaret Weis</name>\n<link><![CDATA[https://www.goodreads.com/author/show/869.Margaret_Weis]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>23606</id>\n<title><![CDATA[J.R.R. Tolkien: Author of the Century]]></title>\n<title_without_series><![CDATA[J.R.R. Tolkien: Author of the Century]]></title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/23606.J_R_R_Tolkien]]></link>\n<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>\n<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>\n<num_pages>384</num_pages>\n<work>\n<id>1069817</id>\n</work>\n<isbn>0618257594</isbn>\n<isbn13>9780618257591</isbn13>\n<average_rating>3.80</average_rating>\n<ratings_count>5126</ratings_count>\n<publication_year>2002</publication_year>\n<publication_month>9</publication_month>\n<publication_day>8</publication_day>\n<authors>\n<author>\n<id>5288510</id>\n<name>Tom Shippey</name>\n<link><![CDATA[https://www.goodreads.com/author/show/5288510.Tom_Shippey]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>5739533</id>\n<title><![CDATA[A Fúria dos Reis (As Crónicas de Gelo e Fogo #3)]]></title>\n<title_without_series>A Fúria dos Reis</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/5739533-a-f-ria-dos-reis]]></link>\n<small_image_url><![CDATA[https://images.gr-assets.com/books/1380290549s/5739533.jpg]]></small_image_url>\n<image_url><![CDATA[https://images.gr-assets.com/books/1380290549m/5739533.jpg]]></image_url>\n<num_pages>429</num_pages>\n<work>\n<id>21892386</id>\n</work>\n<isbn></isbn>\n<isbn13>9789896370268</isbn13>\n<average_rating>4.35</average_rating>\n<ratings_count>5325</ratings_count>\n<publication_year>2008</publication_year>\n<publication_month>2</publication_month>\n<publication_day></publication_day>\n<authors>\n<author>\n<id>346732</id>\n<name>George R.R. Martin</name>\n<link><![CDATA[https://www.goodreads.com/author/show/346732.George_R_R_Martin]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>11100431</id>\n<title><![CDATA[Heir of Novron (The Riyria Revelations, #5-6)]]></title>\n<title_without_series>Heir of Novron</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/11100431-heir-of-novron]]></link>\n<small_image_url><![CDATA[https://images.gr-assets.com/books/1307960132s/11100431.jpg]]></small_image_url>\n<image_url><![CDATA[https://images.gr-assets.com/books/1307960132m/11100431.jpg]]></image_url>\n<num_pages>932</num_pages>\n<work>\n<id>16022532</id>\n</work>\n<isbn>0316187712</isbn>\n<isbn13>9780316187718</isbn13>\n<average_rating>4.47</average_rating>\n<ratings_count>31605</ratings_count>\n<publication_year>2012</publication_year>\n<publication_month>1</publication_month>\n<publication_day>31</publication_day>\n<authors>\n<author>\n<id>2063919</id>\n<name>Michael J. Sullivan</name>\n<link><![CDATA[https://www.goodreads.com/author/show/2063919.Michael_J_Sullivan]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>92003</id>\n<title>The Atlas of Middle-Earth</title>\n<title_without_series>The Atlas of Middle-Earth</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/92003.The_Atlas_of_Middle_Earth]]></link>\n<small_image_url><![CDATA[https://images.gr-assets.com/books/1452027305s/92003.jpg]]></small_image_url>\n<image_url><![CDATA[https://images.gr-assets.com/books/1452027305m/92003.jpg]]></image_url>\n<num_pages>210</num_pages>\n<work>\n<id>1502479</id>\n</work>\n<isbn>0618126996</isbn>\n<isbn13>9780618126996</isbn13>\n<average_rating>4.15</average_rating>\n<ratings_count>10720</ratings_count>\n<publication_year>1991</publication_year>\n<publication_month></publication_month>\n<publication_day></publication_day>\n<authors>\n<author>\n<id>11590</id>\n<name>Karen Wynn Fonstad</name>\n<link><![CDATA[https://www.goodreads.com/author/show/11590.Karen_Wynn_Fonstad]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>1170158</id>\n<title>The Earthsea Trilogy</title>\n<title_without_series>The Earthsea Trilogy</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/1170158.The_Earthsea_Trilogy]]></link>\n<small_image_url><![CDATA[https://images.gr-assets.com/books/1295382040s/1170158.jpg]]></small_image_url>\n<image_url><![CDATA[https://images.gr-assets.com/books/1295382040m/1170158.jpg]]></image_url>\n<num_pages>478</num_pages>\n<work>\n<id>964597</id>\n</work>\n<isbn>0140050930</isbn>\n<isbn13>9780140050936</isbn13>\n<average_rating>4.25</average_rating>\n<ratings_count>16029</ratings_count>\n<publication_year>1979</publication_year>\n<publication_month></publication_month>\n<publication_day></publication_day>\n<authors>\n<author>\n<id>874602</id>\n<name>Ursula K. Le Guin</name>\n<link><![CDATA[https://www.goodreads.com/author/show/874602.Ursula_K_Le_Guin]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>140128</id>\n<title><![CDATA[The Sword and the Flame (The Dragon King, #3)]]></title>\n<title_without_series>The Sword and the Flame</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/140128.The_Sword_and_the_Flame]]></link>\n<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>\n<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>\n<num_pages>320</num_pages>\n<work>\n<id>6395772</id>\n</work>\n<isbn>0745946194</isbn>\n<isbn13>9780745946191</isbn13>\n<average_rating>3.78</average_rating>\n<ratings_count>2196</ratings_count>\n<publication_year>2002</publication_year>\n<publication_month>8</publication_month>\n<publication_day>1</publication_day>\n<authors>\n<author>\n<id>28083</id>\n<name>Stephen R. Lawhead</name>\n<link><![CDATA[https://www.goodreads.com/author/show/28083.Stephen_R_Lawhead]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>45109</id>\n<title>Golden Fool (Tawny Man, #2)</title>\n<title_without_series>Golden Fool</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/45109.Golden_Fool]]></link>\n<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>\n<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>\n<num_pages>709</num_pages>\n<work>\n<id>2960727</id>\n</work>\n<isbn>0553582453</isbn>\n<isbn13>9780553582451</isbn13>\n<average_rating>4.33</average_rating>\n<ratings_count>52584</ratings_count>\n<publication_year>2003</publication_year>\n<publication_month>12</publication_month>\n<publication_day></publication_day>\n<authors>\n<author>\n<id>25307</id>\n<name>Robin Hobb</name>\n<link><![CDATA[https://www.goodreads.com/author/show/25307.Robin_Hobb]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>828878</id>\n<title><![CDATA[The Complete Chronicles of Conan]]></title>\n<title_without_series><![CDATA[The Complete Chronicles of Conan]]></title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/828878.The_Complete_Chronicles_of_Conan]]></link>\n<small_image_url><![CDATA[https://images.gr-assets.com/books/1431298282s/828878.jpg]]></small_image_url>\n<image_url><![CDATA[https://images.gr-assets.com/books/1431298282m/828878.jpg]]></image_url>\n<num_pages>927</num_pages>\n<work>\n<id>146900</id>\n</work>\n<isbn>0575077662</isbn>\n<isbn13>9780575077669</isbn13>\n<average_rating>4.27</average_rating>\n<ratings_count>3404</ratings_count>\n<publication_year>2009</publication_year>\n<publication_month>4</publication_month>\n<publication_day>1</publication_day>\n<authors>\n<author>\n<id>66700</id>\n<name>Robert E. Howard</name>\n<link><![CDATA[https://www.goodreads.com/author/show/66700.Robert_E_Howard]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>39058</id>\n<title><![CDATA[The Gormenghast Novels (Gormenghast, #1-3)]]></title>\n<title_without_series>The Gormenghast Novels</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/39058.The_Gormenghast_Novels]]></link>\n<small_image_url><![CDATA[https://images.gr-assets.com/books/1362402890s/39058.jpg]]></small_image_url>\n<image_url><![CDATA[https://images.gr-assets.com/books/1362402890m/39058.jpg]]></image_url>\n<num_pages>1173</num_pages>\n<work>\n<id>38776</id>\n</work>\n<isbn>0879516283</isbn>\n<isbn13>9780879516284</isbn13>\n<average_rating>3.99</average_rating>\n<ratings_count>7497</ratings_count>\n<publication_year>1995</publication_year>\n<publication_month>12</publication_month>\n<publication_day></publication_day>\n<authors>\n<author>\n<id>22018</id>\n<name>Mervyn Peake</name>\n<link><![CDATA[https://www.goodreads.com/author/show/22018.Mervyn_Peake]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>37684</id>\n<title><![CDATA[The Lion, the Witch and the Wardrobe (Chronicles of Narnia, #1)]]></title>\n<title_without_series><![CDATA[The Lion, the Witch and the Wardrobe]]></title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/37684.The_Lion_the_Witch_and_the_Wardrobe]]></link>\n<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>\n<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>\n<num_pages>48</num_pages>\n<work>\n<id>45074241</id>\n</work>\n<isbn>0060556501</isbn>\n<isbn13>9780060556501</isbn13>\n<average_rating>4.17</average_rating>\n<ratings_count>4496</ratings_count>\n<publication_year>2004</publication_year>\n<publication_month>9</publication_month>\n<publication_day>21</publication_day>\n<authors>\n<author>\n<id>94838</id>\n<name>Hiawyn Oram</name>\n<link><![CDATA[https://www.goodreads.com/author/show/94838.Hiawyn_Oram]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>372299</id>\n<title><![CDATA[The Hitch Hiker's Guide to the Galaxy: A Trilogy in Five Parts]]></title>\n<title_without_series><![CDATA[The Hitch Hiker's Guide to the Galaxy: A Trilogy in Five Parts]]></title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/372299.The_Hitch_Hiker_s_Guide_to_the_Galaxy]]></link>\n<small_image_url><![CDATA[https://images.gr-assets.com/books/1322913171s/372299.jpg]]></small_image_url>\n<image_url><![CDATA[https://images.gr-assets.com/books/1322913171m/372299.jpg]]></image_url>\n<num_pages>776</num_pages>\n<work>\n<id>42028477</id>\n</work>\n<isbn>0434003484</isbn>\n<isbn13>9780434003488</isbn13>\n<average_rating>4.41</average_rating>\n<ratings_count>10977</ratings_count>\n<publication_year>1995</publication_year>\n<publication_month>10</publication_month>\n<publication_day>16</publication_day>\n<authors>\n<author>\n<id>4</id>\n<name>Douglas Adams</name>\n<link><![CDATA[https://www.goodreads.com/author/show/4.Douglas_Adams]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>1667456</id>\n<title><![CDATA[Der Krieg der Zwerge (Die Zwerge, #2)]]></title>\n<title_without_series>Der Krieg der Zwerge</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/1667456.Der_Krieg_der_Zwerge]]></link>\n<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>\n<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>\n<num_pages>608</num_pages>\n<work>\n<id>1662343</id>\n</work>\n<isbn>3492700934</isbn>\n<isbn13>9783492700931</isbn13>\n<average_rating>4.05</average_rating>\n<ratings_count>4177</ratings_count>\n<publication_year>2004</publication_year>\n<publication_month>10</publication_month>\n<publication_day>31</publication_day>\n<authors>\n<author>\n<id>355223</id>\n<name>Markus Heitz</name>\n<link><![CDATA[https://www.goodreads.com/author/show/355223.Markus_Heitz]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>100933</id>\n<title><![CDATA[That Hideous Strength (Space Trilogy, #3)]]></title>\n<title_without_series>That Hideous Strength</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/100933.That_Hideous_Strength]]></link>\n<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>\n<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>\n<num_pages>534</num_pages>\n<work>\n<id>964487</id>\n</work>\n<isbn>0007157177</isbn>\n<isbn13>9780007157174</isbn13>\n<average_rating>3.87</average_rating>\n<ratings_count>25210</ratings_count>\n<publication_year>2005</publication_year>\n<publication_month>12</publication_month>\n<publication_day>5</publication_day>\n<authors>\n<author>\n<id>1069006</id>\n<name>C.S. Lewis</name>\n<link><![CDATA[https://www.goodreads.com/author/show/1069006.C_S_Lewis]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>18879</id>\n<title><![CDATA[The Belgariad, Vol. Two: Castle of Wizardry / Enchanters' End Game (The Belgariad, #4-5)]]></title>\n<title_without_series><![CDATA[The Belgariad, Vol. Two: Castle of Wizardry / Enchanters' End Game]]></title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/18879.The_Belgariad_Vol_Two]]></link>\n<small_image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png]]></small_image_url>\n<image_url><![CDATA[https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png]]></image_url>\n<num_pages>483</num_pages>\n<work>\n<id>938065</id>\n</work>\n<isbn>0345456319</isbn>\n<isbn13>9780345456311</isbn13>\n<average_rating>4.38</average_rating>\n<ratings_count>6751</ratings_count>\n<publication_year>2002</publication_year>\n<publication_month>8</publication_month>\n<publication_day>27</publication_day>\n<authors>\n<author>\n<id>8732</id>\n<name>David Eddings</name>\n<link><![CDATA[https://www.goodreads.com/author/show/8732.David_Eddings]]></link>\n</author>\n</authors>\n</book>\n\n          <book>\n<id>68524</id>\n<title><![CDATA[Enemy of God (The Warlord Chronicles, #2)]]></title>\n<title_without_series>Enemy of God</title_without_series>\n<link><![CDATA[https://www.goodreads.com/book/show/68524.Enemy_of_God]]></link>\n<small_image_url><![CDATA[https://images.gr-assets.com/books/1311985643s/68524.jpg]]></small_image_url>\n<image_url><![CDATA[https://images.gr-assets.com/books/1311985643m/68524.jpg]]></image_url>\n<num_pages>397</num_pages>\n<work>\n<id>66401</id>\n</work>\n<isbn>0312187149</isbn>\n<isbn13>9780312187149</isbn13>\n<average_rating>4.38</average_rating>\n<ratings_count>14010</ratings_count>\n<publication_year>1998</publication_year>\n<publication_month>3</publication_month>\n<publication_day>15</publication_day>\n<authors>\n<author>\n<id>12542</id>\n<name>Bernard Cornwell</name>\n<link><![CDATA[https://www.goodreads.com/author/show/12542.Bernard_Cornwell]]></link>\n</author>\n</authors>\n</book>\n\n  </similar_books>\n</book>\n\n</GoodreadsResponse>"
  end

  let :analysis_body do
    JSON.dump(
      "usage": {
        "text_units": 2,
        "text_characters": 15_566,
        "features": 1
      },
      "sentiment": {
        "document": {
          "score": 0.170845,
          "label": 'positive'
        }
      },
      "retrieved_url": 'https://www.goodreads.com/book/show/39555470-mango-people-in-banana-republic',
      "language": 'en'
    )
  end

  let :book_search do
    stub_request(:get, "https://www.goodreads.com/search/index?format=xml&key=#{ENV['GR_KEY']}&q=lord%20of%20the%20rings")
      .with(
        headers: {
          'Host' => 'www.goodreads.com'
        }
      )
      .to_return(status: 200, body: search_body, headers: {})
  end

  let :book_options do
    stub_request(:post, "https://graph.facebook.com/v2.6/me/messages?access_token=#{ENV['ACCESS_TOKEN']}")
      .with(
        body: JSON.dump( { "recipient": { "id": "#{messenger_id}" }, "message": { "attachment": { "type":"template", "payload": { "template_type": "generic", "image_aspect_ratio": "horizontal", "elements": [ { "title": "The Lord of the Rings (The Lord of the Rings, #1-3)", "image_url": "https://images.gr-assets.com/books/1521850178m/33.jpg", "subtitle": "Goodreads id: 33", "buttons": [ { "type": "postback", "title": "The Lord of the Rings (The Lord of the Rings, #1-3)", "payload": 33 } ] }, { "title": "The Fellowship of the Ring (The Lord of the Rings, #1)", "image_url": "https://images.gr-assets.com/books/1298411339m/34.jpg", "subtitle": "Goodreads id: 34", "buttons": [ { "type": "postback", "title": "The Fellowship of the Ring (The Lord of the Rings, #1)", "payload": 34 } ] }, { "title": "The Two Towers (The Lord of the Rings, #2)", "image_url": "https://images.gr-assets.com/books/1298415523m/15241.jpg", "subtitle": "Goodreads id: 15241", "buttons": [ { "type": "postback", "title": "The Two Towers (The Lord of the Rings, #2)", "payload": 15241 } ] }, { "title": "The Return of the King (The Lord of the Rings, #3)", "image_url": "https://images.gr-assets.com/books/1520258755m/18512.jpg", "subtitle": "Goodreads id: 18512", "buttons": [ { "type": "postback", "title": "The Return of the King (The Lord of the Rings, #3)", "payload": 18512 } ] }, { "title": "The Lord of the Rings: Weapons and Warfare", "image_url": "https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png", "subtitle": "Goodreads id: 36", "buttons": [ { "type": "postback", "title": "The Lord of the Rings: Weapons and Warfare", "payload": 36}]}]}}}}),
        headers: {
          'Content-Type' => 'application/json'
        }
      )
      .to_return(status: 200, body: '', headers: {})
  end

  let :book_find do
    stub_request(:get, "https://www.goodreads.com/book/show?format=xml&id=33&key=#{ENV['GR_KEY']}")
      .with(
        headers: {
          'Host' => 'www.goodreads.com'
        }
      )
      .to_return(status: 200, body: find_body, headers: {})
  end

  let :book_confirmation do
    stub_request(:post, "https://graph.facebook.com/v2.6/me/messages?access_token=#{ENV['ACCESS_TOKEN']}")
      .with(
        body: JSON.dump({ "recipient": { "id": "#{messenger_id}" }, "message": { "attachment": { "type": "template", "payload": { "template_type": "generic", "image_aspect_ratio": "square", "elements": [ { "title": "The Lord of the Rings (The Lord of the Rings, #1-3)", "image_url": "https://images.gr-assets.com/books/1521850178m/33.jpg", "subtitle": "Goodreads id: 33", "default_action": { "type": "web_url", "url": "https://www.goodreads.com/book/show/33.The_Lord_of_the_Rings", "messenger_extensions": false, "webview_height_ratio": "full" }, "buttons": [ { "type": "postback", "title": "Confirm", "payload": "33" }, { "type": "postback", "title": "Cancel", "payload": "ASK_FIRST_QUESTION" }]}]}}}}),
        headers: {
          'Content-Type' => 'application/json'
        }
      )
      .to_return(status: 200, body: '', headers: {})
  end

  let :book_analysis do
    stub_request(:post, 'https://gateway.watsonplatform.net/natural-language-understanding/api/v1/analyze?version=2018-03-16')
      .with(
        body: '{"url":"https://www.goodreads.com/api/reviews_widget_iframe?did=DEVELOPER_ID\\u0026format=html\\u0026isbn=0618640150\\u0026links=660\\u0026min_rating=\\u0026review_back=fff\\u0026stars=000\\u0026text=000","features":{"sentiment":{}}}',
        headers: {
          'Authorization' => 'Basic YjY0Mzk2ZWEtMDQ1Ni00NTI3LWJjZGQtNDIzYjNlNGJkZjNiOmlXYTVpRnBBRjEzSg==',
          'Content-Type' => 'application/json'
        }
      )
      .to_return(status: 200, body: analysis_body, headers: {})
  end

  let :book_evaluation do
    stub_request(:post, "https://graph.facebook.com/v2.6/me/messages?access_token=#{ENV['ACCESS_TOKEN']}")
      .with(
        body: JSON.dump({ "recipient": { "id": "#{messenger_id}" }, "message": { "text": "The Lord of the Rings (The Lord of the Rings, #1-3) has a positive sentiment; between -1 and 1, it scored 0.170845." } }),
        headers: {
          'Content-Type' => 'application/json'
        }
      )
      .to_return(status: 200, body: '', headers: {})
  end

  let :another_evaluation do
    stub_request(:post, "https://graph.facebook.com/v2.6/me/messages?access_token=#{ENV['ACCESS_TOKEN']}")
      .with(
        body: JSON.dump({ "recipient": { "id": "#{messenger_id}" }, "message": { "attachment": { "type": "template", "payload": { "template_type": "button", "text": "Would you like to evaluate another?", "buttons": [ { "type": "postback", "title": "Yes", "payload": "ASK_FIRST_QUESTION" }, { "type": "postback", "title": "No", "payload": "EXIT"}]}}}}),
        headers: {
          'Content-Type' => 'application/json'
        }
      )
      .to_return(status: 200, body: '', headers: {})
  end

  let :typing_on do
    stub_request(:post, "https://graph.facebook.com/v2.6/me/messages?access_token=#{ENV['ACCESS_TOKEN']}").
      with(
        body: JSON.dump({"recipient": { "id": "#{messenger_id}" }, "sender_action": "typing_on" }),
        headers: {
        'Content-Type'=>'application/json'
        }
      ).
      to_return(status: 200, body: "", headers: {})
  end

  let :bad_search_name do
    stub_request(:get, "https://www.goodreads.com/search/index?format=xml&key=#{ENV['GR_KEY']}&q=123qwe123wer345").
    with(
      headers: {
        'Host'=>'www.goodreads.com',
      }).
    to_return(status: 200, body: bad_search_results, headers: {})
  end

  let :bad_search_results do
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<GoodreadsResponse>\n  <Request>\n    <authentication>true</authentication>\n      <key><![CDATA[ZU7ZeXjsNSwNoq0pB6EZA]]></key>\n    <method><![CDATA[search_index]]></method>\n  </Request>\n  <search>\n  <query><![CDATA[gfuihuihkkjgyt876785]]></query>\n    <results-start>1</results-start>\n    <results-end>0</results-end>\n    <total-results>0</total-results>\n    <source>Goodreads</source>\n    <query-time-seconds>0.03</query-time-seconds>\n    <results>\n    </results>\n</search>\n\n</GoodreadsResponse>"
  end

  let :no_results do
    stub_request(:post, "https://graph.facebook.com/v2.6/me/messages?access_token=#{ENV['ACCESS_TOKEN']}").
      with(
        body: JSON.dump({ "recipient": { "id": "#{messenger_id}" }, "message": { "text": "There were no resutls."}}),
        headers: {
          'Content-Type'=>'application/json'
        }
      ).
      to_return(status: 200, body: "", headers: {})
  end

  def alternative_options(attr)
    # let :alternative_options do
      stub_request(:post, "https://graph.facebook.com/v2.6/me/messages?access_token=#{ENV['ACCESS_TOKEN']}").
        with(
          body: JSON.dump({ "recipient": { "id": "#{messenger_id}"}, "message": { "attachment": { "type": "template", "payload": { "template_type": "button", "text": "What would you like to do?", "buttons": [ { "type": "postback", "title": "Try Again", "payload": attr }, { "type": "postback", "title": "Start Over", "payload": "START_USER_SURVEY" }]}}}}),
          headers: {
            'Content-Type'=>'application/json'
          }
        ).
        to_return(status: 200, body: "", headers: {})
    # end
  end

  let :bad_search_id do
    stub_request(:get, "https://www.goodreads.com/book/show?format=xml&id=123qwe123wer345&key=#{ENV['GR_KEY']}").
      with(
        headers: {
          'Host'=>'www.goodreads.com',
        }
      ).
      to_return(status: 404, body: "", headers: {})
  end
end
