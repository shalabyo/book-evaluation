require 'rails_helper'

describe 'bot replies with', type: :request do
  include_context 'API Context'

  context '#valid?' do
    context 'hi and first question' do
      before do
        typing_on
        greet
        first_question
      end

      after do
        expect(typing_on).to have_been_requested
        expect(greet).to have_been_requested
        expect(first_question).to have_been_requested
      end

      context 'after quit' do
        it 'hi' do
          post '/bot', hi.to_json
        end
      end

      context 'first timer' do
        let(:user) { nil }

        it 'get started' do
          get_name
          post '/bot', get_started.to_json
          expect(get_name).to have_been_requested
        end
      end
    end

    context 'search books' do
      before do
        typing_on
        user
      end

      context 'by name' do
        it 'outbound' do
          by_name
          post '/bot', choose_name.to_json
          expect(by_name).to have_been_requested
        end

        it 'inbound' do
          user.by_name!
          book_search
          book_options
          post '/bot', input_name.to_json
          expect(book_search).to have_been_requested
          expect(book_options).to have_been_requested
        end
      end
    
      context 'by id' do
        it 'outbound' do
          by_id
          post '/bot', choose_id.to_json
          expect(by_id).to have_been_requested
        end

        it 'inbound' do
          user.by_id!
          book_find
          book_confirmation
          post '/bot', input_id.to_json
          expect(book_find).to have_been_requested
          expect(book_confirmation).to have_been_requested
        end
      end
    end

    context 'choose book' do
      before do
        typing_on
        book_find
        book_analysis
        book_evaluation
        another_evaluation
      end

      after do
        expect(book_find).to have_been_requested
        expect(book_analysis).to have_been_requested
        expect(book_evaluation).to have_been_requested
        expect(another_evaluation).to have_been_requested
      end

      it 'selects book' do
        post '/bot', select_book.to_json
      end

      it 'confirms book' do
        post '/bot', confirm_book.to_json
      end
    end
    
    context 'cancel confirmation' do
      it 'when searching by id' do
        typing_on
        user
        first_question
        post '/bot', cancel.to_json
        expect(first_question).to have_been_requested
      end
    end
  end

  context '#invalid?' do
    context 'not found' do
      before do
        %w(input_name input_id).each do |attr|
          send(attr)['entry'].first['messaging'].first['message']['text'] = '123qwe123wer345'
        end
        typing_on
        no_results
      end

      after do
        expect(typing_on).to have_been_requested
        expect(no_results).to have_been_requested
      end

      it 'by name' do
        alternative_options("BY_NAME")
        user.by_name!
        bad_search_name
        post '/bot', input_name.to_json
        expect(bad_search_name).to have_been_requested
        expect(alternative_options("BY_NAME")).to have_been_requested
      end

      it 'by_id' do
        alternative_options("BY_ID")
        user.by_id!
        bad_search_id
        post '/bot', input_id.to_json
        expect(bad_search_id).to have_been_requested
        expect(alternative_options("BY_ID")).to have_been_requested
      end
    end
  end
end
