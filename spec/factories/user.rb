FactoryBot.define do
  
  factory :user do
    first_name Faker::Name.first_name
    messenger_id Faker::Number.number(16)
    
    trait :first_question do
      status 'first_question'
    end

    trait :by_name do
      status 'by_name'
    end

    trait :by_id do
      status 'by_id'
    end

    trait :quit do
      status 'quit'
    end
  end
end