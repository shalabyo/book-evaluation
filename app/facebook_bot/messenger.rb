include Facebook::Messenger
require 'facebook/messenger'

Bot.on :message do |message|
  message.typing_on
  @user = user(message.sender['id'])

  case @user.status.to_sym
  when :first_question
    wait_for_input(message)
  when :by_name
    by_name(message)
  when :by_id
    by_id(message)
  when :options
    wait_for_input(message)
  else
    postback = Facebook::Messenger::Incoming::Postback.new('sender' => message.sender, 'recipient' => message.recipient, 'timestamp' => Time.now.to_i, 'postback' => { 'payload' => 'START_USER_SURVEY', referral: { ref: 'my-ref-value', source: 'SHORTLINK', type: 'OPEN_THREAD' } })
    execute_survey(postback)
  end
end

Bot.on :postback do |postback|
  postback.typing_on
  execute_survey(postback)
end

def user(messenger_id)
  User.get(messenger_id)
end

def execute_survey(postback)
  messenger_id = postback.sender['id']
  @user = user(messenger_id)
  answer = postback.payload

  case answer
  when 'START_USER_SURVEY'
    @user.first_question! unless @user.first_question?
    greeting(postback)
  when 'ASK_FIRST_QUESTION'
    @user.first_question!
    ask_first_question(postback)
  when 'BY_NAME'
    @user.by_name!
    postback.reply(text: 'Enter book name')
  when 'BY_ID'
    @user.by_id!
    postback.reply(text: 'Enter book id')
  when 'EXIT'
    @user.quit!
    exit_survey(postback)
  else
    evaluate_book(postback)
  end
end

def wait_for_input(message)
  message.reply(text: 'Please choose an option')
end

def greeting(postback)
  first_name = User.get(postback.sender['id']).first_name
  postback.reply(text: "Hi #{first_name}")

  new_postback = Facebook::Messenger::Incoming::Postback.new('sender' => postback.sender, 'recipient' => postback.recipient, 'timestamp' => Time.now.to_i, 'postback' => { 'payload' => 'ASK_FIRST_QUESTION', referral: { ref: 'my-ref-value', source: 'SHORTLINK', type: 'OPEN_THREAD' } })
  execute_survey(new_postback)
end

def ask_first_question(postback)
  postback.reply(
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text: 'Are you searching for a book by...?',
        buttons: [
          { type: 'postback', title: 'Name', payload: 'BY_NAME' },
          { type: 'postback', title: 'Goodreads ID', payload: 'BY_ID' }
        ]
      }
    }
  )
end

def by_name(message)
  begin
    book_search = Book::Search.by_name(message.text)

    results = book_search.work.first(5)
    message.reply(
      attachment: {
        type: 'template',
        payload: {
          template_type: 'generic',
          image_aspect_ratio: 'horizontal',
          elements: Book::Options.call(results)
        }
      }
    )
  rescue Goodreads::NotFound
    failure('BY_NAME', message)
  end
  user(message.sender['id']).options!
end

def by_id(message)
  begin
    book_search = Book::Search.by_id(message.text)
  rescue Goodreads::NotFound
    failure('BY_ID', message)
  else
    Book::Show.call(book_search, message)
  end
  user(message.sender['id']).options!
end

def evaluate_book(postback)
  messenger_id = postback.sender['id']
  choice_id = postback.payload
  postback.reply(text: Book::Analysis.call(choice_id))

  postback.reply(
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text: 'Would you like to evaluate another?',
        buttons: [
          { type: 'postback', title: 'Yes', payload: 'ASK_FIRST_QUESTION' },
          { type: 'postback', title: 'No', payload: 'EXIT' }
        ]
      }
    }
  )
  user(postback.sender['id']).options!
end

def exit_survey(postback)
  postback.reply(text: 'Thank you, Should you request another evaluation just drop in and say hi.')
end

private

def failure(payload, message)
  message.reply(text: 'There were no resutls.')
  message.reply(
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text: 'What would you like to do?',
        buttons: [
          { type: 'postback', title: 'Try Again', payload: payload },
          { type: 'postback', title: 'Start Over', payload: 'START_USER_SURVEY' }
        ]
      }
    }
  )
end

