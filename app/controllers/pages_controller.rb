class PagesController < ApplicationController
  def privacy
    render template: "pages/#{params[:page]}"
  end
end
