module Book
  module Analysis
    def self.call(choice_id)
      r = Search.by_id(choice_id)
      d = Nokogiri::HTML(r.reviews_widget)
      url = 'https://gateway.watsonplatform.net/natural-language-understanding/api/v1/analyze?version=2018-03-16'

      response = RestClient::Request.execute(
        method: :post,
        url: url,
        user: ENV['IBM_WATSON_USER'],
        password: ENV['IBM_WATSON_PASS'],
        headers: {
          content_type: 'application/json'
        },
        payload: {
          "url": d.css('iframe').first['src'],
          "features": {
            "sentiment": {}
          }
        }.to_json
      )

      response = JSON.parse(response)
      sentiment_label = response['sentiment']['document']['label']
      sentiment_score = response['sentiment']['document']['score']

      "#{r.title} has a #{sentiment_label} sentiment; between -1 and 1, it scored #{sentiment_score}."
    end
  end
end
