module Book
  module Options
    def self.call(results)
      arr = []
      results.each do |b|
        b = b.best_book
        arr << {
          title: b.title,
          image_url: b.image_url,
          subtitle: "Goodreads id: #{b.id}",
          buttons: [
            {
              type: 'postback',
              title: b.title,
              payload: b.id
            }
          ]
        }
      end
      arr
    end
  end
end
