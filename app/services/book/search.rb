module Book
  module Search
    @grclient = Goodreads.new(Goodreads.configuration)

    def self.by_name(params)
      book_search = @grclient.search_books(params).results
      raise Goodreads::NotFound if book_search.blank?
      book_search
    end

    def self.by_id(params)
      @grclient.book(params)
    end
  end
end
