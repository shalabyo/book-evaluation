module Book
  module Show
    def self.call(book_search, message)
      message.reply(
        attachment: {
          type: 'template',
          payload: {
            template_type: 'generic',
            image_aspect_ratio: 'square',
            elements: [
              {
                title: book_search.title,
                image_url: book_search.image_url,
                subtitle: "Goodreads id: #{book_search.id}",
                default_action: {
                  type: 'web_url',
                  url: book_search.url,
                  messenger_extensions: false,
                  webview_height_ratio: 'full'
                },
                buttons: [
                  {
                    type: 'postback',
                    title: 'Confirm',
                    payload: book_search.id
                  },
                  {
                    type: 'postback',
                    title: 'Cancel',
                    payload: 'ASK_FIRST_QUESTION'
                  }
                ]
              }
            ]
          }
        }
      )
      User.get(message.sender['id']).options!
    end
  end
end
