require 'open-uri'

class User < ActiveRecord::Base
  enum status: [:first_question, :by_name, :by_id, :options, :quit]

  def self.get(messenger_id)
    user = User.find_by(messenger_id: messenger_id)
    unless user.try(:persisted?)
      url = "https://graph.facebook.com/v2.6/#{messenger_id}?fields=first_name&access_token=#{ENV['ACCESS_TOKEN']}"
      begin
        user_data = JSON.parse(open(url).read)
      rescue
        # this is needed as the open-uri returns 400 for facebook 
        # app reviewers as they have by default denied access to basic
        # user profile info that is allowed to be retrieved for users,
        # without permissions, forcing devs to use the messenger 
        # extension sdk to get user profile info.
        user_data = { 'first_name' => 'there' }
      end
      user = User.create!(first_name: user_data['first_name'], messenger_id: messenger_id, status: :first_question)
    end
    user
  end
end
