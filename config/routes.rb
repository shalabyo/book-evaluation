Rails.application.routes.draw do
  mount Facebook::Messenger::Server, at: 'bot'
  get "/pages/:page" => "pages#privacy"
end
